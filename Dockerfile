FROM python:3.9-alpine3.15

COPY . .
# Install build deps
RUN apk add --no-cache --virtual .build-deps g++ gcc musl-dev python3-dev libffi-dev openssl-dev cargo &&\
    # Install packages (for what virtualenv? we run on Docker)
    pip install --no-cache-dir --upgrade -r requirements.txt &&\
    # Uninstall build deps
    apk del .build-deps
RUN pip install --no-cache-dir --upgrade -r requirements.txt

EXPOSE 9050
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "9050"]
